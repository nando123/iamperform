<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monotony extends MX_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
		parent::__construct();
		$this->load->model('ModelActivityUser');
		$this->load->model('ModelUsers');
		$this->load->model('ModelSelect');
		$this->load->model('ModelMonotony');
		$this->load->module('selection');
		if(!$this->session->userdata("sessUsername")){
			redirect("login/form");
			return;
		}
	}
	
	public function dataMonotony()
	{
		$username	= $this->session->userdata("sessUsername");
		$name		= $this->session->userdata("sessName");
		$gambar		= $this->session->userdata("sessGambar");
		$role_type	= $this->session->userdata("sessRoleType");
		$role_name	= $this->session->userdata("sessRoleName");
		$group_id	= $this->session->userdata("sessGroupID");
		$data["gambar"]		= $gambar;
		$data["name"] 		= $name;
		$data["username"] 	= $username;
		$data["role_type"]	= $role_type;
		$data["role_name"] 	= $role_name;
		$data["module"] 	= "Training Load";
		// $selection 	= new Selection();
		
		$this->ModelActivityUser->setActivityUser($username,6,8);
		
		if(!$this->session->userdata("month")){
			$month = date("m");
		}else{
			$month = $this->session->userdata("month");
		}
		
		if(!$this->session->userdata("year")){
			$year = date("Y");
		}else{
			$year = $this->session->userdata("year");
		}
		
		if($role_type == "ATL"){
			$this->showMonotony($username,$month,$year);
		}else{
			if(!$this->session->userdata("pickGroupID")){				
				$this->selection->selectGroup($username);
				return;
			}
			if(!$this->session->userdata("sessAtlet")){				
				$this->selection->selectAtlet($this->session->userdata("pickGroupID"));
				return;
			}
			$atletID	= $this->session->userdata("sessAtlet");
			$this->showMonotony($atletID,$month,$year);
		}
		
		$this->session->unset_userdata("msg");
	}
	
	function showMonotony($atletID,$month,$year){
		$username	= $this->session->userdata("sessUsername");
		$name		= $this->session->userdata("sessName");
		$gambar		= $this->session->userdata("sessGambar");
		$role_type	= $this->session->userdata("sessRoleType");
		$role_name	= $this->session->userdata("sessRoleName");
		$group_id	= $this->session->userdata("sessGroupID");
		
		
		$this->session->set_userdata("sessAtlet",$atletID);
		
		$data["gambar"]		= $gambar;
		$data["name"] 		= $name;
		$data["username"] 	= $username;
		$data["role_type"]	= $role_type;
		$data["role_name"] 	= $role_name;
		$data["module"] 	= "Training Load";
		
		$bln=array(1=>"January","February","March","April","May","June","July","August","September","October","November","December");
		$optBulan	= "";
		for($bulan=1; $bulan<=12; $bulan++){
			if($bulan<=9) {
				$tmpBulan = "0$bulan"; 
			}else{
				$tmpBulan = $bulan;
			}
			if($tmpBulan == $month){$slcMonth = "selected";}else{$slcMonth = "";}
			$optBulan .="<option $slcMonth value='$tmpBulan'>$bln[$bulan]</option>"; 
		}
		
		$now	= date("Y");
		$optTahun = "";
		for($i=$now;$i>=2010;$i--){
			if($i == $year){$slcYear = "selected";}else{$slcYear = "";}
			$optTahun	.="<option $slcYear value='$i'>$i</option>";
		}
		
		$atletInfo	= $this->ModelUsers->getAtletInfo($atletID);
		list($atletName,$atletID,$atletGroup,$atletEvent,$atletPic,$atletWellnessValue,$atletWellnessDate) = $atletInfo;
		
		if($role_type == "ATL"){
			$labelAtlet	= $atletName;
			$labelGroup = "";
		}else{
			if($role_type == "CHC"){$groupID = $group_id;}else{$groupID = $this->session->userdata("pickGroupID");}
			$labelAtlet	= $this->ModelSelect->optAtlet($groupID,$atletID);
			$labelGroup = "onClick='changeGroup()'";
			$listGroup 	= $this->ModelSelect->getGroup($username);
		}
		
		$data['labelAtlet'] = $labelAtlet;
		$data['labelGroup'] = $labelGroup;
		$data['atletID'] 	= $atletID;
		$data['atletGroup'] = $atletGroup;
		$data['atletEvent'] = $atletEvent;
		$data['atletPic'] 	= $atletPic;
		$data['optBulan']	= $optBulan;
		$data['optTahun']	= $optTahun;
		$data['atletWellnessValue'] = $atletWellnessValue;
		$data['atletWellnessDate']  = $atletWellnessDate;
		
		$data["monotonyData"]	= $this->monotonyTable($atletID,$month,$year);
		$data["pages"]			= "monotonyData";
		$this->load->view("layout/sidebar",$data);
	}
	
	function monotonyTable($atletID,$month,$year){
		$role_type	= $this->session->userdata("sessRoleType");
		$monotonyData	= $this->ModelMonotony->getMonotony($atletID,$month,$year);
		
		$selection 	= new Selection();
		$bulan		= $selection->month2Ind($month);
		$ret ="<thead><tr><th colspan='4' class='center red white-text'>Training Load $bulan $year</th><tr></thead>";
		if($monotonyData){
			foreach ($monotonyData as $key) {
				$created_dttm	 = $key->created_dttm;
				$user_id		 = $key->created_user_id;
				$monotonyID 	 = $key->monotonyID;
				$startDay		 = $key->monotonyStartDttm;
				$endDay			 = $key->monotonyEndDttm;
				$phase 			 = $key->monotonyPhase;
				$date 			 = date("d M Y H:i:s",strtotime($created_dttm));
				$start 			 = date("d M Y",strtotime($startDay));
				$end 			 = date("d M Y",strtotime($endDay));
				
				$sql	= $this->db->query("SELECT name FROM users WHERE username = '$user_id'");
				if($sql->num_rows()>0){
					$row		= $sql->row();
					$created_user_nm	= $row->name;
				}else{
					$created_user_nm = "";
				}
				
				if($role_type == "KSC"){
					$btnDelete	= '<button onClick="deleteMonotony(\''.$monotonyID.'\')"class="btn-floating waves-effect waves-light red">
                                        <i class="mdi-action-delete"></i>
                                    </button>';
				}else{
					$btnDelete = "";
				}
				
				$query	= $this->db->query(" SELECT SUM(monotonyVolume*monotonyIntensity) as monotonyPerDay FROM master_monotony_detail"
										 . " WHERE monotonyID = '$monotonyID' group by monotonyID");
				if($query->num_rows()>0){
					$row	= $query->row();
					$monotonyPerDay = $row->monotonyPerDay;
				}else{
					$monotonyPerDay	= 0;
				}
				
				$ret .= "
					<tr id='data_$monotonyID'>
						<tr>
							<td class='blue white-text'>Tanggal</td>
							<td class='blue white-text'>Total</td>
							<td class='blue white-text'>$btnDelete</td>
						</tr>
						<tr>
							<td style='font-weight:bold'>
								<a href='#' onClick='goToData(\"$monotonyID\")'>Phase : $phase<br>
								<div class='chip cyan white-text'>
									$start - $end
								</div></a>
							</td>
							<td style='font-weight:bold' colspan='2'>
								$monotonyPerDay
							</td>
						</tr>
						<tr>
							<td colspan='4'>Dibuat Pada $date</td>
						</tr>
						<tr>
							<td colspan='4'>Dibuat Oleh $created_user_nm</td>
						</tr>
					</tr>
				";
			}
		}else{
			$ret .= "<tr><td>Data Kosong</td></tr>";
		}
		
		return $ret;
	}
	
	function filterMonotony(){
		$atletID	= $this->session->userdata("sessAtlet");
		$month		= $_POST["month"];
		$year		= $_POST["year"];
		
		$this->session->set_userdata("month",$month);
		$this->session->set_userdata("year",$year);
		
		$ret	= $this->monotonyTable($atletID,$month,$year);
		echo $ret;
	}
	
	function stepOne(){
		$username	= $this->session->userdata("sessUsername");
		$name		= $this->session->userdata("sessName");
		$gambar		= $this->session->userdata("sessGambar");
		$role_type	= $this->session->userdata("sessRoleType");
		$role_name	= $this->session->userdata("sessRoleName");
		$group_id	= $this->session->userdata("sessGroupID");
		$data["gambar"]		= $gambar;
		$data["name"] 		= $name;
		$data["username"] 	= $username;
		$data["role_type"]	= $role_type;
		$data["role_name"] 	= $role_name;
		$data["module"] 	= "Training Load";
		$selection 	= new Selection();
		
		if(!$this->session->userdata("pickGroupID")){			
			$selection 	= new Selection();
			$this->selection->selectGroup($username);
			return;
		}else{
			$checkBoxAtlet	= $selection->checkBoxAtlet($this->session->userdata("pickGroupID"));
			$selectWeek		= $selection->selectWeek();
			
			$data["selectWeek"]		= $selectWeek;
			$data["checkBoxAtlet"]	= $checkBoxAtlet;
			$data["pages"]			= "stepOne";
			$this->load->view("layout/sidebar",$data);
			
			$this->session->unset_userdata("msg");
		}
	}
	
	function stepTwo(){
		$username	= $this->session->userdata("sessUsername");
		$name		= $this->session->userdata("sessName");
		$gambar		= $this->session->userdata("sessGambar");
		$role_type	= $this->session->userdata("sessRoleType");
		$role_name	= $this->session->userdata("sessRoleName");
		$group_id	= $this->session->userdata("sessGroupID");
		$data["gambar"]		= $gambar;
		$data["name"] 		= $name;
		$data["username"] 	= $username;
		$data["role_type"]	= $role_type;
		$data["role_name"] 	= $role_name;
		$data["module"] 	= "Training Load";
		$weekID			= $this->session->userdata("sessWeek");
		$listWeekByID 	= $this->ModelSelect->listWeekByID($weekID);
		
		list($week,$start_date,$end_date,$year)	= $listWeekByID;
		$this->session->set_userdata("sessStartDate",$start_date);
		$this->session->set_userdata("sessEndDate",$end_date);
		
		$ret = $this->getFormDate($start_date);
		$data["ret"]	= $ret;
		$data["start"]  = date("d M Y", strtotime($start_date));
		$data["end"]  	= date("d M Y", strtotime($end_date));
		$data["pages"]	= "stepTwo";
		$this->load->view("layout/sidebar",$data);
	}

	function getFormDate($tanggal){
		$start_date = $this->session->userdata("sessStartDate");
		$end_date 	= $this->session->userdata("sessEndDate");

		$nextDate 	= date('Y-m-d', strtotime($tanggal . ' +1 day'));
		$phase 	= "";
		if($tanggal == $start_date){
			$phase = '
				<div class="row">
					<div class="input-field col s12">
					<input placeholder="Phase Training Load" name="phase" type="text" required>
					<label for="phase">Phase</label>
					</div>
				</div>
			';
			$phase .= '
				<div class="row">
					<div class="input-field col s12">
					<input placeholder="Target Training Load" name="target" type="text" value="1.5">
					<label for="phase">Target</label>
					</div>
				</div>
			';
		}
		$phase .='
			<div class="row">
				<div class="input-field col s12">
				<p for="phase">Tanggal</p>
				<input readonly type="text" value="'.date("l, d M Y", strtotime($tanggal)).'">
				<input placeholder="Tanggal" id="tanggal" name="tanggal" type="hidden" value="'.$tanggal.'">
				</div>
			</div>
		';
		$ret = '
			<form class="col s12" id="formFirst">
				'.$phase.'
				<div class="row">
					<p><b>Sessi Ke 1</b></p>
					<div class="input-field col s12">
						<input placeholder="Session type" name="session_'.$tanggal.'[]" type="text">
						<label for="phase">Session Type</label>
					</div>
					<div class="form-group col s12">
						<label class="control-label">Effort scale 1 to 10 - RPE</label>
						<select name="rpe_'.$tanggal.'[]" class="browser-default js--animations" required >
							<option value="0"	>0 - Not Set</option>
							<option value="1"	>1 - rest/very very easy</option>
							<option value="2"	>2 - very easy</option>
							<option value="3"	>3 - easy</option>
							<option value="4"	>4 - medium</option>
							<option value="5"	>5 - med - hard</option>
							<option value="6"	>6 - hard</option>
							<option value="7"	>7 - hard - very hard</option>
							<option value="8"	>8 - very hard</option>
							<option value="9"	>9 - very very hard</option>
							<option value="10"	>10 - extremly hard</option>
						</select>
					</div>
					<div class="input-field col s12">
						<input placeholder="60" name="volume_'.$tanggal.'[]" type="number">
						<label for="phase">Duration Volume in minutes</label>
					</div>
				</div>
				<div id="rowspan"></div>
				<div class="row">
					<span class="waves-effect waves-light btn btn-floating green" onClick="addFormMonotony(\''.$tanggal.'\')"><i class="mdi-content-add"></i></span>
				</div><br>
				<div class="row">
					<button class="btn cyan" style="float:right">Next</button>
				</div>
			</form>
			<script>
				$("#formFirst").submit(function(event){
					event.preventDefault();
					var form = $(this);
					var date = "'.$nextDate.'";
					$.ajax({
						type : "POST",
						url  : toUrl+"/monotony/saveMonotonyDetail",
						data : form.serialize(),
						dataType: "json",
						success: function(data){
							if(data.status == "sukses"){
								$.ajax({
									type : "POST",
									url  : toUrl+"/monotony/getFormDateAjax/"+date,
									success: function(data){
										$("#formLoad").html(data);
									},error: function(xhr, ajaxOptions, thrownError){            
										alert(xhr.responseText);
									}
								});
							}else{
								alert("Cek Koneksi Internet Anda");
							}
						},error: function(xhr, ajaxOptions, thrownError){            
							alert(xhr.responseText);
						}
					});	
					
				});
			</script>
		';
		return $ret;
	}

	function saveMonotonyDetail(){
		$user_id = $this->session->userdata("sessUsername");
		$tanggal = $_POST["tanggal"];
		$session = $_POST["session_".$tanggal];
		$rpe 	 = $_POST["rpe_".$tanggal];
		$volume	 = $_POST["volume_".$tanggal];
		$weekID		= $this->session->userdata("sessWeek");
		$arrAtlet	= $this->session->userdata("sessArrAtlet");
		$listWeekByID 	= $this->ModelSelect->listWeekByID($weekID);
		list($week,$start_date,$end_date,$year)	= $listWeekByID;
		if($tanggal == $start_date){
			$save   = $this->ModelMonotony->saveMonotony($arrAtlet,$user_id,$session,$rpe,$volume,$weekID,$tanggal,$end_date,$_POST["phase"],$_POST["target"]);
		}else{
			$save   = $this->ModelMonotony->saveMonotonyDetail($arrAtlet,$user_id,$session,$rpe,$volume,$weekID,$tanggal);
		}

		$array = array("status"=>$save,"tanggal"=>$tanggal);

		echo json_encode($array);
	}

	function getFormDateAjax($tanggal){
		$start_date = $this->session->userdata("sessStartDate");
		$end_date 	= $this->session->userdata("sessEndDate");
		$nextDate 	= date('Y-m-d', strtotime($tanggal . ' +1 day'));

		if($tanggal == $end_date){
			$btn = '<span class="btn light-blue darken-4" style="float:right" onClick="simpanMonotony()">Finish</span>';
		}else{
			$btn = '<button class="btn cyan" style="float:right">Next</button>';
		}

		$phase 	= "";
		if($tanggal == $start_date){
			$phase = '
				<div class="row">
					<div class="input-field col s12">
					<input placeholder="Phase Training Load" name="phase" type="text" required>
					<p for="phase">Phase</p>
					</div>
				</div>
			';
			$phase .= '
				<div class="row">
					<div class="input-field col s12">
					<input placeholder="Target Training Load" name="target" type="text" value="1.5">
					<p for="phase">Target</p>
					</div>
				</div>
			';
		}
		$phase .='
			<div class="row">
				<div class="input-field col s12">
				<p for="phase">Tanggal</p>
				<input readonly type="text" value="'.date("l, d M Y", strtotime($tanggal)).'">
				<input placeholder="Tanggal" id="tanggal" name="tanggal" type="hidden" value="'.$tanggal.'">
				</div>
			</div>
		';
		$ret = '
			<form class="col s12" id="formFirst">
				'.$phase.'
				<div class="row">
					<p><b>Sessi Ke 1</b></p>
					<div class="input-field col s12">
						<input placeholder="Session type" name="session_'.$tanggal.'[]" type="text">
						<label for="phase">Session Type</label>
					</div>
					<div class="form-group col s12">
						<label class="control-label">Effort scale 1 to 10 - RPE</label>
						<select name="rpe_'.$tanggal.'[]" class="browser-default js--animations" required >
							<option value="0"	>0 - Not Set</option>
							<option value="1"	>1 - rest/very very easy</option>
							<option value="2"	>2 - very easy</option>
							<option value="3"	>3 - easy</option>
							<option value="4"	>4 - medium</option>
							<option value="5"	>5 - med - hard</option>
							<option value="6"	>6 - hard</option>
							<option value="7"	>7 - hard - very hard</option>
							<option value="8"	>8 - very hard</option>
							<option value="9"	>9 - very very hard</option>
							<option value="10"	>10 - extremly hard</option>
						</select>
					</div>
					<div class="input-field col s12">
						<input placeholder="60" name="volume_'.$tanggal.'[]" type="number">
						<label for="phase">Duration Volume in minutes</label>
					</div>
				</div>
				<div id="rowspan"></div>
				<div class="row">
					<span class="waves-effect waves-light btn btn-floating green" onClick="addFormMonotony(\''.$tanggal.'\')"><i class="mdi-content-add"></i></span>
				</div><br>
				<div class="row">
					'.$btn.'
				</div>
			</form>
			<script>
				$("#formFirst").submit(function(event){
					event.preventDefault();
					var form = $(this);
					var date = "'.$nextDate.'";
					$.ajax({
						type : "POST",
						url  : toUrl+"/monotony/saveMonotonyDetail",
						data : form.serialize(),
						dataType: "json",
						success: function(data){
							if(data.status == "sukses"){
								$.ajax({
									type : "POST",
									url  : toUrl+"/monotony/getFormDateAjax/"+date,
									data : form.serialize(),
									success: function(data){
										$("#formLoad").html(data);
									},error: function(xhr, ajaxOptions, thrownError){            
										alert(xhr.responseText);
									}
								});
							}else{
								alert("Cek Koneksi Internet Anda");
							}
						},error: function(xhr, ajaxOptions, thrownError){            
							alert(xhr.responseText);
						}
					});	
					
				});

				function simpanMonotony(){
					var tanggal = $("#tanggal").val();
					$.ajax({
						type : "POST",
						url  : toUrl+"/monotony/saveMonotonyDetail",
						data : $("#formFirst").serialize()+ "&tanggal="+tanggal,
						dataType: "json",
						success: function(data){
							if(data.status == "sukses"){
								window.location.href="training";
							}
						},error: function(xhr, ajaxOptions, thrownError){            
							alert(xhr.responseText);
						}
					});
				}
			</script>
		';
		echo $ret;
	}
	
	function addForm(){
		$no = $_POST["no"]+1;
		$tanggal = $_POST["tanggal"];

		$ret = '
			<p><b>Sessi Ke '.$no.'</b> 
			<span onClick="deleteForm(\''.$no.'\')" class="btn-floating waves-effect waves-light red" style="float:right"><i class="mdi-content-remove"></i></span></p>
			<div class="input-field col s12">
				<input placeholder="Session type" name="session_'.$tanggal.'[]" type="text">
				<label for="phase">Session Type</label>
			</div>
			<div class="form-group col s12">
				<label class="control-label">Effort scale 1 to 10 - RPE</label>
				<select name="rpe_'.$tanggal.'[]" class="browser-default js--animations" required >
					<option value="0"	>0 - Not Set</option>
					<option value="1"	>1 - rest/very very easy</option>
					<option value="2"	>2 - very easy</option>
					<option value="3"	>3 - easy</option>
					<option value="4"	>4 - medium</option>
					<option value="5"	>5 - med - hard</option>
					<option value="6"	>6 - hard</option>
					<option value="7"	>7 - hard - very hard</option>
					<option value="8"	>8 - very hard</option>
					<option value="9"	>9 - very very hard</option>
					<option value="10"	>10 - extremly hard</option>
				</select>
			</div>
			<div class="input-field col s12">
				<input placeholder="60" name="volume_'.$tanggal.'[]" type="number">
				<label for="phase">Duration Volume in minutes</label>
			</div>
		';

		echo $ret;
	}

	function stepThree(){
		$username	= $this->session->userdata("sessUsername");
		$name		= $this->session->userdata("sessName");
		$gambar		= $this->session->userdata("sessGambar");
		$role_type	= $this->session->userdata("sessRoleType");
		$role_name	= $this->session->userdata("sessRoleName");
		$group_id	= $this->session->userdata("sessGroupID");
		$data["gambar"]		= $gambar;
		$data["name"] 		= $name;
		$data["username"] 	= $username;
		$data["role_type"]	= $role_type;
		$data["role_name"] 	= $role_name;
		$data["module"] 	= "Training Load";
		
		$weekID			= $this->session->userdata("sessWeek");
		$listWeekByID 	= $this->ModelSelect->listWeekByID($weekID);
		
		list($week,$start,$end,$year)	= $listWeekByID;
		
		$data['form'] = array(
			"start"				=> "start",
			"day" 				=> "day[]",
			"session" 			=> "session[%idx%][]",
			"scale" 			=> "scale[%idx%][]",
			"volume" 			=> "volume[%idx%][]",
			"phase" 			=> "phase",
			"goal" 				=> "goal",
			"description"		=> "description",
			"lastweek" 			=> "lastweek",
			"save" 				=> "save"
		);
		
		$daySet = array("Minggu","Senin","Selasa","Rabu","Kamis","Jum`at","Sabtu");
		$data['data'] = array(
			"start" 	=> $start,
		);
		
		$data['data']['rowcount'] 	= 0;
		$data['data']['date'] 		= array();
		$counter = 0;
		$key 	 = "day";
		while(isset($_POST['day'.$counter])) {

			$w 	= date('w', strtotime($start. ' +'.($counter).' day'));
			
			$data['data']['date'][$counter]['COUNT'] 	= $_POST['day'.$counter];
			$data['data']['date'][$counter]['DAY'] 	= $daySet[$w];
			$data['data']['date'][$counter]['DATE'] 	= date('Y-m-d', strtotime($start. ' +'.($counter).' day'));
			$data['data']['rowcount']  				   += $_POST['day'.$counter];
			$counter++;

		}//END WHILE
		
		$data["pages"]	= "stepThree";
		$this->load->view("layout/sidebar",$data);
	}
	
	function setAtletMonotony(){
		$arrAtlet	= $_POST["atletID"];
		$sessWeek	= $_POST["week"];
		
		$this->session->set_userdata("sessArrAtlet",$arrAtlet);
		$this->session->set_userdata("sessWeek",$sessWeek);
		return true;
	}
	
	function deleteMonotony(){
		$monotonyID	= $_POST["monotonyID"];
		
		$query	= $this->db->query("UPDATE master_monotony SET monotonyActive = '1' WHERE monotonyID = '$monotonyID'");
		
		if($query){
			echo "sukses";
			return;
		}else{
			echo "gagal";
			return;
		}
	}
	
	function setMonotonyID(){
		$monotonyID	= $_POST["monotonyID"];
		$this->session->set_userdata("monotonyID",$monotonyID);
		return true;
	}
	
	function monotonyDataTable(){
		$monotonyID	= $this->session->userdata("monotonyID");
		$username	= $this->session->userdata("sessUsername");
		$name		= $this->session->userdata("sessName");
		$gambar		= $this->session->userdata("sessGambar");
		$role_type	= $this->session->userdata("sessRoleType");
		$role_name	= $this->session->userdata("sessRoleName");
		$group_id	= $this->session->userdata("sessGroupID");
		$data["gambar"]		= $gambar;
		$data["name"] 		= $name;
		$data["username"] 	= $username;
		$data["role_type"]	= $role_type;
		$data["role_name"] 	= $role_name;
		$data["module"] 	= "Training Load";
		if(!$this->session->userdata("pickGroupID")){				
			$this->selection->selectGroup($username);
			return;
		}
		if(!$this->session->userdata("sessAtlet")){				
			$this->selection->selectAtlet($this->session->userdata("pickGroupID"));
			return;
		}
		
		$atletID	= $this->session->userdata("sessAtlet");
		
		$atletInfo	= $this->ModelUsers->getAtletInfo($atletID);
		list($atletName,$atletID,$atletGroup,$atletEvent,$atletPic,$atletWellnessValue,$atletWellnessDate) = $atletInfo;
		if($role_type == "ATL"){
			$labelAtlet	= $atletName;
			$labelGroup = "";
		}else{
			if($role_type == "CHC"){$groupID = $group_id;}else{$groupID = $this->session->userdata("pickGroupID");}
			$labelAtlet	= $this->ModelSelect->optAtlet($groupID,$atletID);
			$labelGroup = "onClick='changeGroup()'";
			$listGroup 	= $this->ModelSelect->getGroup($username);
		}
		
		$data['labelAtlet'] = $labelAtlet;
		$data['labelGroup'] = $labelGroup;
		$data['atletID'] 	= $atletID;
		$data['atletGroup'] = $atletGroup;
		$data['atletEvent'] = $atletEvent;
		$data['atletPic'] 	= $atletPic;
		$data['monotonyID'] = $monotonyID;
		$data['atletWellnessValue'] = $atletWellnessValue;
		$data['atletWellnessDate']  = $atletWellnessDate;
		$monotonyData	= $this->ModelMonotony->getMonotonyByID($monotonyID);
		$trmnt			= $this->ModelMonotony->getMonotonyTarget($monotonyID);
		if($trmnt){
			list($target,$last)= $trmnt;
		}else{
			$target = '';
			$last 	= '';
		}
			$desc	= '';
		
		$cell = array(
				"value" 		=> array(),
				"wtl" 			=> 0
		);
		if($monotonyData){
			$sample 				= array();
			$TotalScaleVolume 		= 0;
			foreach($monotonyData as $num => $row){
				$mdldate[$num] 		= $row->monotonyDetailDate;
				$sessType[$num] 	= $row->monotonyDetailSession;
				$detIntensity[$num]	= $row->monotonyIntensity;
				$detVolume[$num]	= $row->monotonyVolume;
				$detTrainingLoad[$num] = $detIntensity[$num] * $detVolume[$num];
				if(!isset($cell['value'][$row->monotonyDetailDate])) {
					$cell['value'][$row->monotonyDetailDate]['count'] = 1;
					$cell['value'][$row->monotonyDetailDate]['trainingday']  	= 0 + $detTrainingLoad[$num];
					$sample[$row->monotonyDetailDate] 							= 0 + $detTrainingLoad[$num];
				}else {
					$cell['value'][$row->monotonyDetailDate]['count'] 			+= 1;
					$cell['value'][$row->monotonyDetailDate]['trainingday'] 	+= $detTrainingLoad[$num];
					$sample[$row->monotonyDetailDate] 							+= $detTrainingLoad[$num];
				}
			}
			$STDEV 		= round($this->ModelMonotony->STDEV($sample));
			$TOTAL 		= ceil($this->ModelMonotony->SUM($sample));
			$AVERAGE 	= ceil($this->ModelMonotony->AVERAGE($sample));
			if($AVERAGE==0 OR $STDEV==0) {
				$DIV 	= 0;
			}else {
				$DIV 	= $AVERAGE / $STDEV;
			}//END ELSE IF
			$VARIATION 	= round( $DIV, 1 );
			$LOAD 		= ceil($TOTAL * $VARIATION);

		}
		$data['variation']	= $VARIATION;
		$data['target']		= $target;
		$data['desc']		= $desc;
		$data['load']		= $LOAD;
		$data['total']		= $TOTAL;
		$data['last']		= $last;
		$data['average']	= $AVERAGE;
		$data['stdev']		= $STDEV;
		$data['cell']		= $cell;
		$data['table']		= $monotonyData;
		$data['monotony_id'] = $monotonyID;
		
		$data["pages"]		= "dataTable";
		$this->load->view("layout/sidebar",$data);
	}
	
	function chartSessionMonotony(){
		$montonyID = $_POST['monotonyID'];
		$table 	= $this->ModelMonotony->getMonotonyByID($montonyID);
		if($table){
			foreach($table as $row){
				$volume = $row->monotonyVolume;
				$date	= $row->monotonyDetailDate;
				$rpe	= $row->monotonyIntensity;
				$actual	= $row->rpeActual;
				
				$load	= $rpe*$volume;
				
				$arrLoad[] 		= (int)$load;
				$arrdate[]		= date("d M Y", strtotime($date));
				$arrRpe[]		= (int)$rpe;
				$arrActual[]	= (int)$actual;
			}
			$arrayData = array('categories'=>$arrdate, 'load'=>$arrLoad, 'rpe'=>$arrRpe, 'actual'=>$arrActual);
			$json = json_encode($arrayData);
			
			echo $json;
			return;
		}
	}
	
	function chartDayMonotony(){
		$montonyID = $_POST['monotonyID'];
		$table 	= $this->ModelMonotony->getGrafikDay($montonyID);
		if($table){
			foreach($table as $row){
				$rpe		= $row->detail_intensity;
				$actual		= $row->detail_actual;
				$date		= $row->detail_date;				
				$load		= $row->trainingLoad;
				
				$arrLoad[] 		= (int)$load;
				$arrdate[]		= date("d M Y", strtotime($date));
				$arrRpe[]		= (int)$rpe;
				$arrActual[]	= (int)$actual;
			}
			$arrayData = array('categories'=>$arrdate, 'load'=>$arrLoad, 'rpe'=>$arrRpe, 'actual'=>$arrActual);
			$json = json_encode($arrayData);
			
			echo $json;
			return;
		}
	}
	
	function monotonyChart(){
		$year	= $_POST["year"];
		$month	= $_POST["month"];
		$atlet	= $this->session->userdata("sessAtlet");
		$monotony = $this->ModelMonotony->getMonotony($atlet,$month,$year);
		
		if($monotony){
			foreach ($monotony as $key) {
				$date 			= $key->created_dttm;
				$monotony_id 	= $key->monotonyID;
				$startMonotony	= $key->monotonyStartDttm;
				$endMonotony	= $key->monotonyEndDttm;
				$vdt = date("Y-m-d",strtotime($date));
				$start			= date("d M Y",strtotime($startMonotony));
				$end			= date("d M Y",strtotime($endMonotony));
				
				$query	= $this->db->query(" SELECT SUM(monotonyVolume*monotonyIntensity) as monotonyPerDay FROM `master_monotony_detail`"
									 . " WHERE monotonyID = '$monotony_id' group by monotonyID");
				if($query->num_rows()>0){
					$row	= $query->row();
					$monotonyPerDay = $row->monotonyPerDay;
				}else{
					$monotonyPerDay	= 0;
				}
				
				$arrDate[]		= $start."-".$end;
				$arrVolume[]	= (int)$monotonyPerDay;
			}
			
			$arrayData = array('categories'=>$arrDate, 'volume'=>$arrVolume);
			$json = json_encode($arrayData);
			
			echo $json;
			return;
		}else{
			return false;
		}
	}
}
