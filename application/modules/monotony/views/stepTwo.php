<div class="container">
<!--chart dashboard start-->
	<div id="chart-dashboard">
		<div class="row">
			<div class="col s12 m12 l6">
				<div class="card-panel">
                    <h4 class="header2"><b>Training Load</b> <br><?php echo $start ?> s/d <?php echo $end ?></h4>
					<hr>
					<div class="row" id="formLoad">
						<?php echo $ret ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url()?>appsource/js/modules/typeHandle.js"></script>