<div class="container">
	<!--chart dashboard start-->
	<div id="chart-dashboard">
		<div class="row">
			<div class="col s12 m6 l4">
				<div id="flight-card" class="card">
					<div class="card-content white">
						<div class="row flight-state-wrapper">
							<div class="col s12 m12 l12 center-align">
								<div class="flight-state">
									<h1 class="margin"><i class="mdi-hardware-keyboard-alt" style="color:cyan;font-size:150px;margin-top:100px"></i></h1>
									<h5 style="font-size:20px;margin-top:100px">Wellness Untuk Hari Ini Telah Dibuat</h5>
									<a href="<?=base_url()?>wellness" class="btn red">Go To Wellness</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--work collections end-->

</div>