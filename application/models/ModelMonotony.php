<?php
class ModelMonotony extends CI_Model {
	
	function getMonotonyByID($monotonyID){
		$sql = " SELECT a.*,b.* FROM `master_monotony_detail` as a "
			 . " LEFT JOIN (SELECT * FROM master_pmc WHERE pmcType = 'core' AND pmcCouchType = 'Physic') as b"
			 . " on b.monotonyDetailID = a.monotonyDetailID"
			 . " WHERE a.monotonyID = '$monotonyID'"
			 . " ORDER BY monotonyDetailDate ASC";
		$query = $this->db->query($sql);
		if($query -> num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
		
	function getMonotonyTarget($monotony_id){
		$sql = " SELECT monotonyTarget, monotonyLastWeek FROM master_monotony"
			  ." WHERE monotonyID = '$monotony_id';";
		$query = $this->db->query($sql);
		if($query -> num_rows() > 0){
			$row = $query->row();
			$target = $row->monotonyTarget;
			$last	= $row->monotonyLastWeek;
			
			return array($target,$last);
		}else{
			return false;
		}
	}
	
	function getMonotony($atletID,$month,$year){
		$sql = " SELECT * FROM master_monotony "
				. " WHERE monotonyAtletID = '$atletID' AND MONTH(monotonyStartDttm) = '$month'"
				. " AND YEAR(monotonyStartDttm) = '$year' AND monotonyActive = '0'"
				. " ORDER BY monotonyID DESC";
		$query = $this->db->query($sql);
		if($query -> num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function cekIsExist($week,$atlet){
		$sql = " SELECT monotonyID FROM master_monotony "
			 . " WHERE monotonyAtletID = '$atlet'"
			 . " AND monotonyWeek='$week'"
			 . " AND monotonyActive = '0'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			$row = $query->row();
			$monotony_id = $row->monotonyID;
			$status = "existed";
		}else{
			$monotony_id = "";
			$status = "empty";
		}
		return array($status,$monotony_id);
	}
		
	function saveMonotony($arrAtlet,$user_id,$session,$rpe,$volume,$weekID,$StartDay,$EndDay,$phase,$target){
		$this->db->trans_begin();
		$role_type	= $this->session->userdata("sessRoleType");
		for($k=0; $k<count($arrAtlet); $k++) {
			$cekIsExist = $this->cekIsExist($weekID,$arrAtlet[$k]);
			list($status,$monotonyID)=$cekIsExist;
			
			$query	= $this->db->query("SELECT MAX(monotonyID) as monotonyID FROM master_monotony WHERE monotonyAtletID = '{$arrAtlet[$k]}'");
			
			if($query->num_rows()>0){
				$row	= $query->row();
				$monotonyID = $row->monotonyID;
				
				$cek = $this->db->query("SELECT SUM(monotonyIntensity*monotonyVolume) as dailyMonotony FROM master_monotony_detail WHERE monotonyID = '{$monotonyID[$k]}'");
				
				if($cek->num_rows()>0){
					$key	= $cek->result();
					$tmpM	= "";
					$row = $cek->row();
					$dailyMonotony = $row->dailyMonotony;
				}
			}else{
				$dailyMonotony = 0;
			}

			if($status == "existed"){
				$monotony_id = $monotonyID;
			}else{
				$monotony_id = $this->getMonotonyID();
				$sql = " INSERT INTO master_monotony (monotonyID,created_user_id,monotonyTarget,monotonyPhase,monotonyWeek"
					. " ,monotonyLastWeek,created_dttm,monotonyAtletID,monotonyStartDttm,monotonyEndDttm,monotonyActive)"
					. " values ('$monotony_id','$user_id','$target','$phase','$weekID'"
					. " ,'$dailyMonotony',now(),'{$arrAtlet[$k]}','$StartDay','$EndDay','0')";
				$query = $this->db->query($sql);
			}
			
			$dttm = $StartDay." ".date("H:i:s");
			
			for($i=0; $i<count($session); $i++) {
					
				$w 		= date('w', 		strtotime($StartDay. ' +'.($i).' day'));
				// $Date 	= date('Y-m-d', 	strtotime($StartDay. ' +'.($i).' day'));
					
				// for($j=0; $j<count($session[$i]); $j++) {
					$detail_id = $this->uniqekey();	
					$sqldetail = " INSERT INTO master_monotony_detail (monotonyDetailID,monotonyID,monotonyDay,monotonyDetailUser,"
								." monotonyDetailDate,monotonyDetailSession,monotonyIntensity,monotonyVolume,monotonyLoadPerSession,monotonyType) "
								." values ('$detail_id','$monotony_id','$w','$user_id','$StartDay','{$session[$i]}',"
								." '{$rpe[$i]}','{$volume[$i]}','{$rpe[$i]}*{$volume[$i]}','$role_type')";
					$qDetail = $this->db->query($sqldetail);
				// }
			}
		}
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return "gagal";
		}
		else
		{
			$this->db->trans_commit();
			return "sukses";
		}
	}

	function saveMonotonyDetail($arrAtlet,$user_id,$session,$rpe,$volume,$weekID,$StartDay){
		$this->db->trans_begin();
		$role_type	= $this->session->userdata("sessRoleType");
		for($k=0; $k<count($arrAtlet); $k++) {
			$cekIsExist = $this->cekIsExist($weekID,$arrAtlet[$k]);
			list($status,$monotonyID)=$cekIsExist;
			for($i=0; $i<count($session); $i++) {
					
				$w 		= date('w', 		strtotime($StartDay. ' +'.($i).' day'));
					
				$detail_id = $this->uniqekey();	
				$sqldetail = " INSERT INTO master_monotony_detail (monotonyDetailID,monotonyID,monotonyDay,monotonyDetailUser,"
							." monotonyDetailDate,monotonyDetailSession,monotonyIntensity,monotonyVolume,monotonyLoadPerSession,monotonyType) "
							." values ('$detail_id','$monotonyID','$w','$user_id','$StartDay','{$session[$i]}',"
							." '{$rpe[$i]}','{$volume[$i]}','{$rpe[$i]}*{$volume[$i]}','$role_type')";
				$qDetail = $this->db->query($sqldetail);
			}
		}
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return "gagal";
		}
		else
		{
			$this->db->trans_commit();
			return "sukses";
		}
	}
		
	function uniqekey() {
		$tr = "DETA_";
		$year = date("Y");
		$month = date("m");
		$day = date("d");
		$sql = "SELECT left(a.monotonyDetailID,5) as tr, mid(a.monotonyDetailID,6,4) as fyear," 
				. " mid(a.monotonyDetailID,10,2) as fmonth, mid(a.monotonyDetailID,12,2) as fday,"
				. " right(a.monotonyDetailID,4) as fno FROM master_monotony_detail AS a"
				. " where left(a.monotonyDetailID,5) = '$tr' and mid(a.monotonyDetailID,6,4) = '$year'"
				. " and mid(a.monotonyDetailID,10,2) = '$month' and mid(a.monotonyDetailID,12,2)= '$day'"
				. " order by fyear desc, CAST(fno AS SIGNED) DESC LIMIT 1";
				
		$result = $this->db->query($sql);	
			
		if($result->num_rows($result) > 0) {
			$row = $result->row();
			$tr = $row->tr;
			$fyear = $row->fyear;
			$fmonth = $row->fmonth;
			$fday = $row->fday;
			$fno = $row->fno;
			$fno++;
		} else {
			$tr = $tr;
			$fyear = $year;
			$fmonth = $month;
			$fday = $day;
			$fno = 0;
			$fno++;
		}
		if (strlen($fno)==1){
			$strfno = "000".$fno;
		} else if (strlen($fno)==2){
			$strfno = "00".$fno;
		} else if (strlen($fno)==3){
			$strfno = "0".$fno;
		} else if (strlen($fno)==4){
			$strfno = $fno;
		}
		
		$id_goal = $tr.$fyear.$fmonth.$fday.$strfno;

		return $id_goal;
	}

	function getMonotonyID(){
		$tr = "MONO_";
		$year = date("Y");
		$month = date("m");
		$day = date("d");
		$sql = "SELECT left(a.monotonyID,5) as tr, mid(a.monotonyID,6,4) as fyear," 
				. " mid(a.monotonyID,10,2) as fmonth, mid(a.monotonyID,12,2) as fday,"
				. " right(a.monotonyID,4) as fno FROM master_monotony AS a"
				. " where left(a.monotonyID,5) = '$tr' and mid(a.monotonyID,6,4) = '$year'"
				. " and mid(a.monotonyID,10,2) = '$month' and mid(a.monotonyID,12,2)= '$day'"
				. " order by fyear desc, CAST(fno AS SIGNED) DESC LIMIT 1";
				
		$result = $this->db->query($sql);	
			
		if($result->num_rows($result) > 0) {
			$row = $result->row();
			$tr = $row->tr;
			$fyear = $row->fyear;
			$fmonth = $row->fmonth;
			$fday = $row->fday;
			$fno = $row->fno;
			$fno++;
		} else {
			$tr = $tr;
			$fyear = $year;
			$fmonth = $month;
			$fday = $day;
			$fno = 0;
			$fno++;
		}
		if (strlen($fno)==1){
			$strfno = "000".$fno;
		} else if (strlen($fno)==2){
			$strfno = "00".$fno;
		} else if (strlen($fno)==3){
			$strfno = "0".$fno;
		} else if (strlen($fno)==4){
			$strfno = $fno;
		}
		
		$id_goal = $tr.$fyear.$fmonth.$fday.$strfno;

		return $id_goal;
	}
	
	function STDEV($sample) {
		if(is_array($sample)){
			$mean = array_sum($sample) / count($sample);
			foreach($sample as $key => $num) $devs[$key] = pow($num - $mean, 2);
			$stdev	= sqrt(array_sum($devs) / (count($devs) - 1));
			return $stdev;
		}else{
			return NULL;
		}//END IF
	}

	function AVERAGE($sample) {
		if(is_array($sample)){
			return array_sum($sample) / count($sample);
		}else{
			return NULL;
		}//END IF
	}

	function SUM($sample) {
		if(is_array($sample)){
			return array_sum($sample);
		}else{
			return NULL;
		}//END IF
	}

	function ABS($number) {
		return abs($number);
	}
		
	function getGrafikDay($monotony_id){
		$sql = " SELECT a.monotonyDetailDate as detail_date, SUM(a.monotonyIntensity * a.monotonyVolume) as trainingLoad,"
			 . " SUM(a.monotonyIntensity) as detail_intensity, SUM(b.rpeActual) as detail_actual"
			 . " FROM `master_monotony_detail` as a"
			 . " LEFT JOIN (SELECT * FROM master_pmc WHERE pmcType = 'core' AND pmcCouchType = 'Physic') as b"
			 . " on b.monotonyDetailID = a.monotonyDetailID"
			 . " WHERE a.monotonyID = '$monotony_id'"
			 . " GROUP BY a.monotonyDetailDate";
		$query = $this->db->query($sql);
		if($query -> num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
?>