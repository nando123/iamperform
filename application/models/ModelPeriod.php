<?php
class ModelPeriod extends CI_Model {
	
	function getPeriod($minggu,$year,$atletID){
		$sql 	= "SELECT * FROM `master_period` as a WHERE a.atlet_id = '$atletID' AND a.week = '$minggu' AND a.year = '$year'";
		$query 	= $this->db->query($sql);
		if($query->num_rows()>0){
			$row = $query->row();
			$period_id 	= $row->period_id;
			$week 		= $row->week;
			$year		= $row->year;
			$phy_prep	= $row->phy_prep;
			$tech_prep	= $row->tech_prep;
			$tact_prep	= $row->tact_prep;
			$psyc_prep	= $row->psyc_prep;
			$volume		= $row->volume;
			$intensity	= $row->intensity;
			$peaking	= $row->peaking;
			

			return array($period_id,$week,$year,$phy_prep,$tech_prep,$tact_prep,$psyc_prep,$volume,$intensity,$peaking); 
		}else{
			return false;
		}
	}
}

?>